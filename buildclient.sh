#!/usr/bin/env bash

#To build the client container and add it to the cluster, then expose its port

echo "--- First Build the docker image"
docker build -t client mongo-client/

echo "--- Then import it locally to test with kind, otherwise we can push it to Docker hub and pull it from there"
kind load docker-image client 

echo "--- Finally apply to a kind kubernetes cluster"
kubectl apply -f mongo-client/kubernetes

echo "--- Expose port"
kubectl port-forward svc/mongodb-client 5000:5000