#!/usr/bin/env bash

#To build the server container and add it to the cluster

echo "--- First Build the docker image"
cd mongo-server
docker build -t server .

echo "--- Then import it locally to test with kind"
kind load docker-image server 

echo "--- Finally apply to a kind kubernetes cluster"
cd ..

sleep(10s)
kubectl apply -f mongo-server/kubernetes