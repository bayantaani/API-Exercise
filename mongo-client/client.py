#########################################################
#         MongoDB client implementation                 #
#       of endpoints GET, POST, PUT, DELETE             #
#########################################################

from flask import Flask, jsonify, request 
from flask_pymongo import PyMongo
from uuid import uuid4
from bson.objectid import ObjectId
import bson
from uuid import UUID

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'titanic'
app.config['MONGO_URI'] = 'mongodb://mongodb:27017/titanic' 
#          use 'mongodb://mongodb:27017/titanic' if running in a k8s cluster
#     OR   use 'mongodb://127.0.0.1:27017/titanic' if running on localhost
mongo = PyMongo(app)

# GET all people
@app.route('/people', methods=['GET'])
def get_people():
    people = mongo.db.people
    output = []
    for p in people.find():
        retrieved_person = construct_output(p)
        output.append(retrieved_person)
    return jsonify({'People' : output})

# POST a person to the database
@app.route('/people', methods=['POST'])
def add_person():
    people = mongo.db.people
    new_person  = read_data()
    new_person = people.find_one({'_id': people.insert(new_person)})
    output = construct_output(new_person)
    return jsonify({'Inserted Person' : output})

# DELETE a person
@app.route('/people/<uuid>', methods=['DELETE'])
def delete_person(uuid):
    people = mongo.db.people
    result = people.find_one_and_delete({'_id': ObjectId(uuid)})

    if result is not None:
        output = construct_output(result)
        return jsonify({'Deleted Person' : output})
    else:
        return jsonify({'result' : 'No document found with this uuid'})

# PUT update a person
@app.route('/people/<uuid>', methods=['PUT'])
def update_person(uuid):
    people = mongo.db.people
    filter = people.find_one({'_id': ObjectId(uuid)})

    if filter is not None:
        new_data = read_data()
        new_values = { '$set': new_data}
        people.update_one(filter, new_values)
        return jsonify({'Updated Person' : new_data})
    else:
        return jsonify({'result': 'No document found with this uuid'})


# GET information about one person
@app.route('/people/<uuid>', methods=['GET'])
def get(uuid):
    people = mongo.db.people
    p = people.find_one({'_id': ObjectId(uuid)})

    if p is not None:
        output = construct_output(p)
        return jsonify({'Person' : output})
    else:
        return jsonify({'result':'No document found with this uuid'})

def read_data():
    survived = request.json['Survived']
    passengerClass = request.json['Pclass']
    name = request.json['Name']
    sex = request.json['Sex']
    age = request.json['Age']
    siblingsOrSpousesAboard = request.json['Siblings/Spouses Aboard']
    parentsOrChildrenAboard = request.json['Parents/Children Aboard']
    fare = request.json['Fare']

    return {'Survived': survived, 'Pclass': passengerClass, 'Name': name, 'Sex' : sex, 'Age' : age,
        'Siblings/Spouses Aboard': siblingsOrSpousesAboard,
        'Parents/Children Aboard': parentsOrChildrenAboard,
        'Fare': fare}

# Construct output
def construct_output(p):
    output = {
            'uuid' : str(ObjectId(p['_id'])),
            'Survived' : p['Survived'],
            'Pclass' : p['Pclass'],
            'Name' : p['Name'],
            'Sex' : p['Sex'],
            'Age' : p['Age'],
            'Siblings/Spouses Aboard' : p['Siblings/Spouses Aboard'],
            'Parents/Children Aboard' : p['Parents/Children Aboard'],
            'Fare' : p['Fare']
    }
    return output

### Main method
if __name__ == '__main__':
    app.run(debug=True)
