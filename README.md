# API Exercise
This project is an implementation of a REST API that allows a user to send queries to a database that has info about the passengers of Titanic.

## Before you Start
If you want to start everything in one command, you will need to install a few tools (instructions are for MacOS):
1. Docker:
   ```sh
   brew cask install docker
   ```
2. Kind:
   ```sh
   brew install kind
   ```
3. Kubectl:
   ```sh
   brew install kubernetes-cli
   ```

## To Run the Cluster
Simply run the script `run.sh`:
```sh
./run.sh
```
Please refer to the script for an explanation on running the setup.

### Testing
For easy testing, we suggest you use Postman: https://www.postman.com/

Download the app, create a user, and then you can start testing the API.


## Architecture

![alt text](img/mongo.png "Architecture")

The end user may connect to the front-end service that implements the REST API on port 5000, which in turn communicates with the backend database in order to retrieve the documents on port 27017, which is the default for MongoDB.

## Implementation Details
#### Importing the database into the container image
The Titanic passenger info is saved in [Titanic.csv](`titanic.csv`).

The backend server, which hosts the database, is running in a Docker container (image: [bitnami/minideb](https://hub.docker.com/r/bitnami/minideb/)). To import `titanic.csv` automatically at startup:
1. [bitnami's Dockerfile](https://github.com/bitnami/bitnami-docker-nginx/blob/master/1.19/debian-10/Dockerfile) is modified to copy `titanic.csv` to the container image
2. The following two scripts from the original bitnami repository were modified to have `titanic.csv` imported into MongoDB:
   1. [libmongodb.sh](https://github.com/bitnami/bitnami-docker-mongodb/blob/master/4.4/debian-10/rootfs/opt/bitnami/scripts/libmongodb.sh)
   2. [run.sh](https://github.com/bitnami/bitnami-docker-mongodb/blob/master/4.4/debian-10/rootfs/opt/bitnami/scripts/mongodb/run.sh)

### API Code
The front-end client uses the Flask and Flask-Pymongo packages to implement 5 REST functionalities, namely GET, POST, GET (1 person), DELETE (a person), and PUT (update a person).

It connects to the back-end database server on port 5000 to query and retrieve JSON-formatted data.

### Kubernetes Cluster
In this setup, the tool `kind` is used to create the Kubernetes cluster and load the Docker images.

