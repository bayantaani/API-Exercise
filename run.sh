#!/usr/bin/env bash

echo "Creating Kubernetes cluster"
kind create cluster

echo "Building and running database in K8s cluster"
echo "--- First Build the docker image"
cd mongo-server
docker build -t server .

echo "--- Then import it locally to test with kind"
kind load docker-image server 

echo "--- Finally apply to a kind kubernetes cluster"
cd ..
kubectl apply -f mongo-server/kubernetes

echo "Building and running client in K8s cluster, then exposing port 5000"
echo "--- First Build the docker image"
docker build -t client mongo-client/

echo "--- Then import it locally to test with kind, otherwise we can push it to Docker hub and pull it from there"
kind load docker-image client 

echo "--- Finally apply to a kind kubernetes cluster"
kubectl apply -f mongo-client/kubernetes

echo "--- Waiting for 10 seconds until the pod is running"
sleep 10s

echo "--- Expose port"
kubectl port-forward svc/mongodb-client 5000:5000
